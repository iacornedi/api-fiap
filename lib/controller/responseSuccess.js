class responseSuccess{
    constructor(){
        this.response =  {
            success: null,
            message: null,
            data: {}
        }
    }


    semDadosParaAtualizar(){
        this.response.success = true;
        this.response.message = "Sem dados para atualizar";
        this.response.status = 201;
        this.response.data = {
            message: "As informções já existem na base de dados"
        }

        return this.response
    }

    tokenGeradoComSucesso(token){
        this.response.success = true;
        this.response.message = `Zodiaco-Token criado com sucesso`;
        this.response.data = {
            'x-zodiaco-token' : {
                token : token
            }
        }

        return this.response;
    }

    cadastradoComSucesso(cavaleiro){
        this.response.success = true;
        this.response.message = `A inclusão de "${cavaleiro.nome}" foi efetuada com sucesso`;
        this.response.data = {
            cavaleiro : {
                id : cavaleiro._id,
                nome : cavaleiro.nome,
                patente: cavaleiro.patente,
                ataques: cavaleiro.ataques,
                descricao: cavaleiro.descricao,
                deus: cavaleiro.deus
            }
        }
        return this.response;
    }

    dadosEncontrados(cavaleiro){
        let arr_cavaleiro = []
        this.response.success = true;
        this.response.message = `Dados encontrados`;                  
        for (let i in cavaleiro){
            let objCavaleiro = {}
            objCavaleiro.id = cavaleiro[i]._id
            objCavaleiro.nome = cavaleiro[i].nome
            objCavaleiro.patente = cavaleiro[i].patente
            objCavaleiro.ataques = cavaleiro[i].ataques
            objCavaleiro.descricao = cavaleiro[i].descricao
            objCavaleiro.deus = cavaleiro[i].deus
            
            arr_cavaleiro.push(objCavaleiro);
        }   
        this.response.data = {
            cavaleiro : arr_cavaleiro
        }
        return this.response;
    }

    dadoEncontrado(cavaleiro){        
        this.response.success = true;
        this.response.message = `Dado encontrado`;
        this.response.data = {
            cavaleiro : {
                id : cavaleiro._id,
                nome : cavaleiro.nome,
                patente: cavaleiro.patente,
                ataques: cavaleiro.ataques,
                descricao: cavaleiro.descricao,
                deus: cavaleiro.deus

            }
        }  
        return this.response
    }

    dadosNaoEncontrados(){
        this.response.success = true;
        this.response.message = `Requisição feita com sucesso mas não foram encontradas informações`;
        this.response.data = {}

        return this.response;
    }

    dadoAtualizado(cavaleiro){        
        this.response.success = true;
        this.response.message = `Dado atualizado`;
        this.response.data = {
            cavaleiro : {
                id : cavaleiro._id,
                nome : cavaleiro.nome,
                patente: cavaleiro.patente,
                ataques: cavaleiro.ataques,
                descricao: cavaleiro.descricao,
                deus: cavaleiro.deus

            }
        }  
        return this.response
    }

    dadoRemovido(cavaleiro){        
        this.response.success = true;
        this.response.message = `Dado removido`;
        this.response.data = {
            cavaleiro : {
                id : cavaleiro._id,
                nome : cavaleiro.nome,
                patente: cavaleiro.patente,
                ataques: cavaleiro.ataques,
                descricao: cavaleiro.descricao,
                deus: cavaleiro.deus

            }
        }  
        return this.response
    }
}

module.exports = responseSuccess;