const jwt = require('jsonwebtoken');
require('dotenv').config();

class Autenticacao {
    gerarNovotoken (id,nome,usuario,senha){     
        let usuarioToken = {
            id: id,
            nome: nome.length,
            usuario: usuario,
            senha: senha
        }           
        let token = jwt.sign(usuarioToken, process.env.API_TOKEN, {
            expiresIn: process.env.API_TOKEN_VALIDO
        });
        return token
    }
}

module.exports = Autenticacao;