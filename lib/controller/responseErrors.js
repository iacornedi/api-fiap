class responseErrors {
    constructor(){
        this.response = { 
            errors:{},
            title: null,
            status: null,
            detail: null,
            instance: null
        }
    }

    credenciaisNaoInformadas(endpoint){
        this.response.errors.autenticacao = "Credenciais não informadas";
        this.response.title = "Falha na autenticação";
        this.response.status = 401;
        this.response.detail = "informe usuario e senha";
        this.response.instance = endpoint

        return this.response
    }

    falhaNaAutenticacao(endpoint){
        this.response.errors.autenticacao = "Usuario ou senha invalido";
        this.response.title = "Falha na autenticação";
        this.response.status = 401;
        this.response.detail = "Verifique se o usuário e senha estão correstos";
        this.response.instance = endpoint

        return this.response
    }

    nenhumTokenFornecido(endpoint){
        this.response.errors.autenticacao = "Nenhum token fornecido";
        this.response.title = "Falha na autenticação";
        this.response.status = 401;
        this.response.detail = "Não foi fornecido o toke para a autenticação";
        this.response.instance = endpoint

        return this.response
    }

    tokenInvalido(endpoint){
        this.response.errors.autenticacao = "Token invalido";
        this.response.title = "Falha na autenticação";
        this.response.status = 401;
        this.response.detail = "O token fornecido é invalido ou excedeu o tempo limite";
        this.response.instance = endpoint

        return this.response
    }

    valorObrigatorio(objeto,camposObrigatorios,endpoint){                      
        if(Object.keys(objeto).length == 0){
            for (let i in camposObrigatorios){
                this.response.errors[camposObrigatorios[i]] = [`O valor ${camposObrigatorios[i]} é obrigatório`];
                this.response.status = 400;
            }           
        }else{        
            for(let i in camposObrigatorios){
                if(objeto[camposObrigatorios[i]] == undefined){
                    this.response.errors[i] = [`A chave ${camposObrigatorios[i]} é obrigatório`];
                    this.response.status = 400;
                } 
            }
            for(let i in objeto){     
                if(camposObrigatorios.indexOf(i) != -1){                
                    if(objeto[i] != undefined){
                        if(objeto[i].length == 0){
                            this.response.errors[i] = [`O valor de ${i} é obrigatório`];
                            this.response.status = 400;
                        } 
                    }else{
                        this.response.errors[i] = [`A chave ${i} é obrigatória`];
                        this.response.status = 400;
                    }
                }     
            }
        }
        if(this.response.status == 400){
            this.response.title = "Um ou mais valores não foram informados";
            this.response.detail = "Por favor, verifique qual campo não foi informado ou se ele encontra-se em branco";
            this.response.instance = endpoint;
        }
        return this.response;
    }

    arrayObrigatorio(objeto,camposObrigatorios,endpoint){
        for(let i in objeto){
            if(camposObrigatorios.indexOf(i) != -1){
                if(!Array.isArray(objeto[i])){
                    this.response.errors[i] = [`O valor ${i} deve ser uma Array`];
                    this.response.status = 400;
                }
            }            
        }
        if(this.response.status == 400){
            this.response.title = "Um ou mais valores devem ser do tipo Array";
            this.response.detail = "Por favor, verifique se o tipo do campo está correto";
            this.response.instance = endpoint;
        }
        return this.response;       
    }
}

module.exports = responseErrors;