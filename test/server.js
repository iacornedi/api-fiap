const mongoose = require('mongoose');
const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify : false
}
const chai = require('chai');
const subSet = require('chai-subset');
const http = require('chai-http');
const server = require('../server');

chai.use(subSet);
chai.use(http);

const tokenSchema = {
    success: success => success,
    message: message => message,
    data: data => data
}

const responseErrorsSchema = {
    title: title => title,
    status: status => status,
    detail: detail => detail,
    instance: instance => instance
}


describe('Testes de INTEGRACAO', () => {
    /** Será preenchido quando login tiver sucesso */
    let zodiaco_token = null;
    before('connect', function(){
        return mongoose.createConnection('mongodb://localhost/apidozodiaco',options);
    });    

    it('POST - /login - RETORNO de usuario ou senha CORRETO', function(done){
        chai.request(server).post('/login').send({
            usuario: 'iacornedi',
            senha: 'rm334457' //--> Senha valida
        }).end(function(err,res){
            zodiaco_token = res.body.data['x-zodiaco-token'].token;
            chai.expect(res).to.have.status(200);
            chai.expect(res.body).to.containSubset(tokenSchema);            
            chai.expect(res.body.data['x-zodiaco-token'].token).to.a('String');
            done();
        });
    });

    /**esse nao esta executando */
    it('POST - /login - RETORNO de usuario ou senha INCORRETO',function(done){
        chai.request(server).post('/login').send({
            usuario: 'iacornedi',
            senha: 'rm334457ABC' //--> Senha invalida
        }).end(function(err,res){  
            chai.expect(res).to.have.status(401);         
            chai.expect(res.body).to.containSubset(responseErrorsSchema);
            done();
        });
    });

    it('GET - /cavaleiro?nome - (token obrigadorio)',function(done){
        chai.request(server).get('/cavaleiro?nome=Ikki').set('x-zodiaco-token', zodiaco_token)
            .end(function(err,res){
                // console.log(res.body.data); //--> retorno da api
                chai.expect(res).to.have.status(200);
                chai.expect(res.body.data.cavaleiro).to.a('array');
                chai.expect(res.body.data.cavaleiro[0]['id']).to.a('String');
                chai.expect(res.body.data.cavaleiro[0]['nome']).to.a('String');
                chai.expect(res.body.data.cavaleiro[0]['patente']).to.a('String');
                chai.expect(res.body.data.cavaleiro[0]['ataques']).to.a('array');
                chai.expect(res.body.data.cavaleiro[0]['descricao']).to.a('String');
                chai.expect(res.body.data.cavaleiro[0]['deus']).to.a('String');         
                done();
        });
    });

    it('GET - /cavaleiro/id/:id - (token obrigadorio)',function(done){
        chai.request(server).get('/cavaleiro/id/5e0f260aa8d036e2f66dc91d').set('x-zodiaco-token', zodiaco_token)
            .end(function(err,res){
                // console.log(res.body.data); //--> retorno da api
                chai.expect(res).to.have.status(200);
                chai.expect(res.body.data.cavaleiro).to.a('Object');
                chai.expect(res.body.data.cavaleiro['id']).to.a('String');
                chai.expect(res.body.data.cavaleiro['nome']).to.a('String');
                chai.expect(res.body.data.cavaleiro['patente']).to.a('String');
                chai.expect(res.body.data.cavaleiro['ataques']).to.a('array');
                chai.expect(res.body.data.cavaleiro['descricao']).to.a('String');
                chai.expect(res.body.data.cavaleiro['deus']).to.a('String');         
                done();
        });
    });

    it('GET - /cavaleiro/patente/:patente - (token obrigadorio)',function(done){
        chai.request(server).get('/cavaleiro/patente/prata').set('x-zodiaco-token', zodiaco_token)
            .end(function(err,res){
                // console.log(res.body.data); //--> retorno da api
                chai.expect(res).to.have.status(200);
                chai.expect(res.body.data.cavaleiro).to.a('array');
                chai.expect(res.body.data.cavaleiro[0]['id']).to.a('String');
                chai.expect(res.body.data.cavaleiro[0]['nome']).to.a('String');
                chai.expect(res.body.data.cavaleiro[0]['patente']).to.a('String');
                chai.expect(res.body.data.cavaleiro[0]['ataques']).to.a('array');
                chai.expect(res.body.data.cavaleiro[0]['descricao']).to.a('String');
                chai.expect(res.body.data.cavaleiro[0]['deus']).to.a('String');        
                done();
        });
    });

    it('PUT - /cavaleiro - Atualizar item completo (token obrigadorio)',function(done){
        chai.request(server).put('/cavaleiro').set('x-zodiaco-token', zodiaco_token)
            .send({ 
                "id": "5e0f267aa8d036e2f66dc925",
                "nome": "Aiolos de Sagitário da Silva",
                "patente": "ouro",
                "ataques": [
                    "Atomic Thunderbolt - Trovão Atômico"
                ],
                "descricao": "Cavaleiro de Ouro de Sagitário no Sec. XX, o grego Aioros foi o salvador de Athena das mãos de Saga de Gêmeos, quando aquela ainda era bebê. Mas por armação de Saga, foi declarado traidor do Santuário e julgado como o que tentou matar Athena. O sagitariano foge com o bebê e é perseguido, sendo gravemente ferido pela excalibur de Shura de Capricórnio. Antes de morrer em decorrência dos ferimentos, Aioros encontra o empresário japonês Mitsumasa Kido. Ele explica a situação para Mitsumasa lhe confiando a bebê Athena e a urna com a Armadura de Sagitário, morrendo pouco depois. Além de suas habilidades de luta, tinha excelente personalidade, caráter, humildade e grande senso de justiça, passos seguidos por seu irmão mais novo, Aiolia de Leão. Ele pode ser considerado um verdadeiro exemplo de Cavaleiro de Athena por continuar a protegê-la, bem como a ajudar seus companheiros, mesmo após a morte. Tem o título de Um Cavaleiro Exemplar e sua casa é conhecida como O Templo do Centauro (人馬宮 Jinbakyū?).",
                "deus": "Athena"
            })
            .end(function(err,res){
                // console.log(res.body.data); //--> retorno da api
                chai.expect(res).to.have.status(200);
                chai.expect(res.body.data.cavaleiro).to.a('Object');
                chai.expect(res.body.data.cavaleiro['nome']).to.a('String');
                chai.expect(res.body.data.cavaleiro['patente']).to.a('String');
                chai.expect(res.body.data.cavaleiro['ataques']).to.a('array');
                chai.expect(res.body.data.cavaleiro['descricao']).to.a('String');
                chai.expect(res.body.data.cavaleiro['deus']).to.a('String');        
                done();
        });
    });

    it('PUT - /cavaleiro - ID não informado (token obrigadorio)',function(done){
        chai.request(server).put('/cavaleiro').set('x-zodiaco-token', zodiaco_token)
            .send({ 
                "nome": "Aiolos de Sagitário da Silva",
                "patente": "ouro",
                "ataques": [
                    "Atomic Thunderbolt - Trovão Atômico"
                ],
                "descricao": "Cavaleiro de Ouro de Sagitário no Sec. XX, o grego Aioros foi o salvador de Athena das mãos de Saga de Gêmeos, quando aquela ainda era bebê. Mas por armação de Saga, foi declarado traidor do Santuário e julgado como o que tentou matar Athena. O sagitariano foge com o bebê e é perseguido, sendo gravemente ferido pela excalibur de Shura de Capricórnio. Antes de morrer em decorrência dos ferimentos, Aioros encontra o empresário japonês Mitsumasa Kido. Ele explica a situação para Mitsumasa lhe confiando a bebê Athena e a urna com a Armadura de Sagitário, morrendo pouco depois. Além de suas habilidades de luta, tinha excelente personalidade, caráter, humildade e grande senso de justiça, passos seguidos por seu irmão mais novo, Aiolia de Leão. Ele pode ser considerado um verdadeiro exemplo de Cavaleiro de Athena por continuar a protegê-la, bem como a ajudar seus companheiros, mesmo após a morte. Tem o título de Um Cavaleiro Exemplar e sua casa é conhecida como O Templo do Centauro (人馬宮 Jinbakyū?).",
                "deus": "Athena"
            })
            .end(function(err,res){
                // console.log(res.body); //--> retorno da api
                chai.expect(res).to.have.status(400);
                chai.expect(res.body).to.a('Object');
                chai.expect(res.body.errors.id).to.a('Array');
                chai.expect(res.body.title).to.a('String');
                chai.expect(res.body.detail).to.a('String');
                chai.expect(res.body.instance).to.a('String');     
                done();
        });
    });

    it('PATCH - /cavaleiro - Atualizar campo (token obrigadorio)',function(done){
        chai.request(server).patch('/cavaleiro').set('x-zodiaco-token', zodiaco_token)
            .send({ 
                "id": "5e0f267aa8d036e2f66dc925",
                "nome": "Aiolos de Sagitário da Silva",
                "deus": "Athena da silva"
            })
            .end(function(err,res){
                // console.log(res.body.data); //--> retorno da api
                chai.expect(res).to.have.status(200);
                chai.expect(res.body.data.cavaleiro).to.a('Object');
                chai.expect(res.body.data.cavaleiro['id']).to.a('String');
                chai.expect(res.body.data.cavaleiro['nome']).to.a('String');
                chai.expect(res.body.data.cavaleiro['patente']).to.a('String');
                chai.expect(res.body.data.cavaleiro['ataques']).to.a('array');
                chai.expect(res.body.data.cavaleiro['descricao']).to.a('String');
                chai.expect(res.body.data.cavaleiro['deus']).to.a('String');        
                done();
        });
    });

    it('PATCH - /cavaleiro - Atualizacao já existente (token obrigadorio)',function(done){
        chai.request(server).patch('/cavaleiro').set('x-zodiaco-token', zodiaco_token)
            .send({ 
                "id": "5e0f267aa8d036e2f66dc925",
                "nome": "Aiolos de Sagitário da Silva",
                "deus": "Athena da silva"
            })
            .end(function(err,res){
                chai.expect(res).to.have.status(201);
                chai.expect(res.body).to.a('Object');
                chai.expect(res.body.message).to.a('String');
                chai.expect(res.body.data).to.a('Object');
                chai.expect(res.body.data.message).to.a('String');
                done();
        });
    });

});