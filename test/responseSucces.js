const chai = require('chai');
const subSet = require('chai-subset');

const responseSuccess = require('../lib/controller/responseSuccess');

chai.use(subSet);

const responseSuccessSchema =  {
    success: success => success,
    message: message => message,
    data: data => data
}

describe('Teste UNITARIO - classe responseSuccess',function(){

    it('tokenGeradoComSucesso - retorna token',function(){
        let retornaSucesso = new responseSuccess;
        let token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MzQwLCJ0aW1lc3RhbXAiOjEyNDU3ODk0MjE2NDcsImlhdCI6MTU3ODI1MzcyMH0.6MdWLHWUfev-aM0X9nKOzpvQFMvB2MGrTe9gonZwvC0';
        let retornaToken = retornaSucesso.tokenGeradoComSucesso(token);
        
        chai.expect(retornaToken).to.containSubset(responseSuccessSchema);
        chai.expect(retornaToken.data['x-zodiaco-token'].token).to.a('String')
    });

    it('cadastradoComSucesso - retorna cadastro completo', function(){
        let retornaSucesso = new responseSuccess;        
        let cavaleiro = {
            nome : "Seiya de Pégaso",
            patente: "bronze",
            ataques: [ 
                "Pegasus Ryūsei Ken - Meteoros de Pégaso", 
                "Pegasus Suisei Ken - Cometa de Pégaso", 
                "Pegasus Rolling Crush - Turbilhão de Pégaso"
            ],
            descricao: "Seiya é o Cavaleiro de Bronze da constelação de Pegasus. Quando criança, Seiya foi separado de sua irmã, Seika, para fazer o treinamento para Cavaleiro no Santuário de Athena, na Grécia. Graças aos métodos rígidos e disciplinares de sua mentora Marin de Águia, Seiya foi capaz de cultivar um enorme potencial e conquistar a Armadura de Pégaso. Motivado pelo desejo de reencontrar sua irmã, Seiya depois descobre seu destino como um verdadeiro Cavaleiro de Athena, renascendo sempre que a deusa reencarna, para ajudá-la na batalha contra o mal que consome a Terra. Guerreiro de imenso poder, Seiya alcança a vitória em batalhas aparentemente impossíveis e consegue derrotar até mesmo os deuses Poseidon e Hades.",
            deus: "Athena"
        }

        let novoCavaleiro = retornaSucesso.cadastradoComSucesso(cavaleiro);
        chai.expect(novoCavaleiro).to.containSubset(responseSuccessSchema);
    })

    it('dadoEncontrado - retorna busca completa de 1 resultado', function(){
        let retornaSucesso = new responseSuccess;        
        let cavaleiro = {
            _id : "5e0cf3354ce15507c003c571",
            nome : "Seiya de Pégaso",
            patente: "bronze",
            ataques: [ 
                "Pegasus Ryūsei Ken - Meteoros de Pégaso", 
                "Pegasus Suisei Ken - Cometa de Pégaso", 
                "Pegasus Rolling Crush - Turbilhão de Pégaso"
            ],
            descricao: "Seiya é o Cavaleiro de Bronze da constelação de Pegasus. Quando criança, Seiya foi separado de sua irmã, Seika, para fazer o treinamento para Cavaleiro no Santuário de Athena, na Grécia. Graças aos métodos rígidos e disciplinares de sua mentora Marin de Águia, Seiya foi capaz de cultivar um enorme potencial e conquistar a Armadura de Pégaso. Motivado pelo desejo de reencontrar sua irmã, Seiya depois descobre seu destino como um verdadeiro Cavaleiro de Athena, renascendo sempre que a deusa reencarna, para ajudá-la na batalha contra o mal que consome a Terra. Guerreiro de imenso poder, Seiya alcança a vitória em batalhas aparentemente impossíveis e consegue derrotar até mesmo os deuses Poseidon e Hades.",
            deus: "Athena"
        }

        let dadoEncontrado = retornaSucesso.dadoEncontrado(cavaleiro);
        chai.expect(dadoEncontrado).to.containSubset(responseSuccessSchema);
    })

    it('dadosEncontrados - retorna busca completa de 1 resultado', function(){
        let retornaSucesso = new responseSuccess;        
        let cavaleiro = {
            _id : "5e0cf3354ce15507c003c571",
            nome : "Seiya de Pégaso",
            patente: "bronze",
            ataques: [ 
                "Pegasus Ryūsei Ken - Meteoros de Pégaso", 
                "Pegasus Suisei Ken - Cometa de Pégaso", 
                "Pegasus Rolling Crush - Turbilhão de Pégaso"
            ],
            descricao: "Seiya é o Cavaleiro de Bronze da constelação de Pegasus. Quando criança, Seiya foi separado de sua irmã, Seika, para fazer o treinamento para Cavaleiro no Santuário de Athena, na Grécia. Graças aos métodos rígidos e disciplinares de sua mentora Marin de Águia, Seiya foi capaz de cultivar um enorme potencial e conquistar a Armadura de Pégaso. Motivado pelo desejo de reencontrar sua irmã, Seiya depois descobre seu destino como um verdadeiro Cavaleiro de Athena, renascendo sempre que a deusa reencarna, para ajudá-la na batalha contra o mal que consome a Terra. Guerreiro de imenso poder, Seiya alcança a vitória em batalhas aparentemente impossíveis e consegue derrotar até mesmo os deuses Poseidon e Hades.",
            deus: "Athena"
        }

        let dadosEncontrados = retornaSucesso.dadosEncontrados(cavaleiro);
        chai.expect(dadosEncontrados).to.containSubset(responseSuccessSchema);
    })

    it('dadosEncontrados - retorna busca completa de 2 resultados', function(){
        let retornaSucesso = new responseSuccess;        
        let cavaleiro = [{
                _id : "5e0cf3354ce15507c003c571",
                nome : "Seiya de Pégaso",
                patente: "bronze",
                ataques: [ 
                    "Pegasus Ryūsei Ken - Meteoros de Pégaso", 
                    "Pegasus Suisei Ken - Cometa de Pégaso", 
                    "Pegasus Rolling Crush - Turbilhão de Pégaso"
                ],
                descricao: "Seiya é o Cavaleiro de Bronze da constelação de Pegasus. Quando criança, Seiya foi separado de sua irmã, Seika, para fazer o treinamento para Cavaleiro no Santuário de Athena, na Grécia. Graças aos métodos rígidos e disciplinares de sua mentora Marin de Águia, Seiya foi capaz de cultivar um enorme potencial e conquistar a Armadura de Pégaso. Motivado pelo desejo de reencontrar sua irmã, Seiya depois descobre seu destino como um verdadeiro Cavaleiro de Athena, renascendo sempre que a deusa reencarna, para ajudá-la na batalha contra o mal que consome a Terra. Guerreiro de imenso poder, Seiya alcança a vitória em batalhas aparentemente impossíveis e consegue derrotar até mesmo os deuses Poseidon e Hades.",
                deus: "Athena"
            },
            {
                _id : "5e0e034cb17b5fc1b93a6665",
                nome : "Hyoga de Cisne",
                patente : "bronze",
                descricao : "Hyoga é o Cavaleiro de Bronze da constelação de Cygnus. Nascido no pequeno vilarejo chamado Kohoutek, no leste da Sibéria (Rússia, que na época do lançamento de Saint Seiya era parte da extinta União Soviética). Seu domínio sobre o Cosmo lhe permite criar gelo e neve a temperaturas próximas do zero absoluto. Tal fato, a princípio, parece criar nele certa arrogância. Sendo cristão ortodoxo, Hyoga guarda consigo um rosário com uma cruz que representa a Cruz do Norte (outro nome para sua constelação protetora), única lembrança deixada por sua mãe antes de morrer. De natureza calma, ele normalmente não demonstra suas emoções, mas em determinadas ocasiões aflora um lado mais sentimental.",
                ataques : [ 
                    "Diamond Dust - Pó de Diamante", 
                    "Kol'tso - Círculo de Gelo", 
                    "Kholodnyi Smerch - Turbilhão de Gelo", 
                    "Aurora Thunder Attack - Trovão Aurora Ataque", 
                    "Aurora Execution - Execução Aurora", 
                    "Freezing Coffin - Esquife de Gelo"
                ],
                deus : "Athena"
            }]
        

        let dadosEncontrados = retornaSucesso.dadosEncontrados(cavaleiro);
        chai.expect(dadosEncontrados)
            .to.have.property('data')
            .to.have.property('cavaleiro')
            .with.lengthOf(2)
        chai.expect(dadosEncontrados).to.containSubset(responseSuccessSchema);
    });

    it('dadosNaoEncontrados - Informa que o dado não foi localizado', function(){
        let retornaSucesso = new responseSuccess;        
        let dadosNaoEncontrados = retornaSucesso.dadosNaoEncontrados();
        chai.expect(dadosNaoEncontrados).to.containSubset(responseSuccessSchema);
    });

    it('semDadosParaAtualizar - Informa que não há dados para atualizar', function(){
        let retornaSucesso = new responseSuccess;        
        let dadosNaoEncontrados = retornaSucesso.semDadosParaAtualizar();
        chai.expect(dadosNaoEncontrados).to.containSubset(responseSuccessSchema);
    });
    

    it('dadoAtualizado - retorna informações do registro atualizado', function(){
        let retornaSucesso = new responseSuccess;        
        let cavaleiro = {
            _id : "5e0cf3354ce15507c003c571",
            nome : "Seiya de Pégaso",
            patente: "bronze/ouro/divino",
            ataques: [ 
                "Pegasus Ryūsei Ken - Meteoros de Pégaso", 
                "Pegasus Suisei Ken - Cometa de Pégaso", 
                "Pegasus Rolling Crush - Turbilhão de Pégaso"
            ],
            descricao: "Seiya é o Cavaleiro de Bronze da constelação de Pegasus. Quando criança, Seiya foi separado de sua irmã, Seika, para fazer o treinamento para Cavaleiro no Santuário de Athena, na Grécia. Graças aos métodos rígidos e disciplinares de sua mentora Marin de Águia, Seiya foi capaz de cultivar um enorme potencial e conquistar a Armadura de Pégaso. Motivado pelo desejo de reencontrar sua irmã, Seiya depois descobre seu destino como um verdadeiro Cavaleiro de Athena, renascendo sempre que a deusa reencarna, para ajudá-la na batalha contra o mal que consome a Terra. Guerreiro de imenso poder, Seiya alcança a vitória em batalhas aparentemente impossíveis e consegue derrotar até mesmo os deuses Poseidon e Hades.",
            deus: "Athena"
        }

        let dadoAtualizado = retornaSucesso.dadoAtualizado(cavaleiro);
        chai.expect(dadoAtualizado).to.containSubset(responseSuccessSchema);
    });
    
});