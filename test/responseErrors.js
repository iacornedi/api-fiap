const chai = require('chai');
const subSet = require('chai-subset');

const responseErrors = require('../lib/controller/responseErrors');

chai.use(subSet);

const responseErrorsSchema = {
    errors: errors => errors,
    title: title => title,
    status: status => status,
    detail: detail => detail,
    instance: instance => instance
}

describe('Teste UNITARIO - classe responseErrors', function(){
    it('falhaNaAutenticacao - Falha na autenticação',function(){
        let validarErros = new responseErrors;
        let endpoint = 'test/responseErrors';
        let falhaLogin = validarErros.falhaNaAutenticacao(endpoint);
        chai.expect(falhaLogin).to.containSubset;
    });

    it('valorObrigatorio - Todos os campos informados',function(){
        let validarErros = new responseErrors;
        let cavaleiro = {    
            nome : "Seiya de Pégaso",
            patente : "bronze",
            descricao : "Seiya é o Cavaleiro de Bronze da constelação de Pegasus. Quando criança, Seiya foi separado de sua irmã, Seika, para fazer o treinamento para Cavaleiro no Santuário de Athena, na Grécia. Graças aos métodos rígidos e disciplinares de sua mentora Marin de Águia, Seiya foi capaz de cultivar um enorme potencial e conquistar a Armadura de Pégaso. Motivado pelo desejo de reencontrar sua irmã, Seiya depois descobre seu destino como um verdadeiro Cavaleiro de Athena, renascendo sempre que a deusa reencarna, para ajudá-la na batalha contra o mal que consome a Terra. Guerreiro de imenso poder, Seiya alcança a vitória em batalhas aparentemente impossíveis e consegue derrotar até mesmo os deuses Poseidon e Hades.",
            ataques : [ 
                "Pegasus Ryūsei Ken - Meteoros de Pégaso", 
                "Pegasus Suisei Ken - Cometa de Pégaso", 
                "Pegasus Rolling Crush - Turbilhão de Pégaso"
            ],
            deus : "Athena"
        }
    
        let campoObrigatorio = ['nome', 'patente', 'descricao', 'ataques', 'deus'];
        let endpoint = 'test/responseErrors';
        let valorObrigatorio = validarErros.valorObrigatorio(cavaleiro,campoObrigatorio,endpoint);
        chai.expect(valorObrigatorio).to.have.property('errors').with.empty
        chai.expect(valorObrigatorio).to.have.property('title').with.null
        chai.expect(valorObrigatorio).to.have.property('status').with.null
        chai.expect(valorObrigatorio).to.have.property('detail').with.null
        chai.expect(valorObrigatorio).to.have.property('instance').with.null
    });

    it('valorObrigatorio - Uma chave não foi informado',function(){
        let validarErros = new responseErrors;
        let cavaleiro = {    
            nome : "Seiya de Pégaso",
            descricao : "Seiya é o Cavaleiro de Bronze da constelação de Pegasus. Quando criança, Seiya foi separado de sua irmã, Seika, para fazer o treinamento para Cavaleiro no Santuário de Athena, na Grécia. Graças aos métodos rígidos e disciplinares de sua mentora Marin de Águia, Seiya foi capaz de cultivar um enorme potencial e conquistar a Armadura de Pégaso. Motivado pelo desejo de reencontrar sua irmã, Seiya depois descobre seu destino como um verdadeiro Cavaleiro de Athena, renascendo sempre que a deusa reencarna, para ajudá-la na batalha contra o mal que consome a Terra. Guerreiro de imenso poder, Seiya alcança a vitória em batalhas aparentemente impossíveis e consegue derrotar até mesmo os deuses Poseidon e Hades.",
            ataques : [ 
                "Pegasus Ryūsei Ken - Meteoros de Pégaso", 
                "Pegasus Suisei Ken - Cometa de Pégaso", 
                "Pegasus Rolling Crush - Turbilhão de Pégaso"
            ],
            deus : "Athena"
        }
    
        let campoObrigatorio = ['nome', 'patente', 'descricao', 'ataques', 'deus'];
        let endpoint = 'test/responseErrors';
        let valorObrigatorio = validarErros.valorObrigatorio(cavaleiro,campoObrigatorio,endpoint);

        chai.expect(valorObrigatorio).to.containSubset(responseErrorsSchema);
    });

    it('valorObrigatorio - Um valor está vazio',function(){
        let validarErros = new responseErrors;
        let cavaleiro = {    
            nome : "Seiya de Pégaso",
            patente : "",
            descricao : "Seiya é o Cavaleiro de Bronze da constelação de Pegasus. Quando criança, Seiya foi separado de sua irmã, Seika, para fazer o treinamento para Cavaleiro no Santuário de Athena, na Grécia. Graças aos métodos rígidos e disciplinares de sua mentora Marin de Águia, Seiya foi capaz de cultivar um enorme potencial e conquistar a Armadura de Pégaso. Motivado pelo desejo de reencontrar sua irmã, Seiya depois descobre seu destino como um verdadeiro Cavaleiro de Athena, renascendo sempre que a deusa reencarna, para ajudá-la na batalha contra o mal que consome a Terra. Guerreiro de imenso poder, Seiya alcança a vitória em batalhas aparentemente impossíveis e consegue derrotar até mesmo os deuses Poseidon e Hades.",
            ataques : [ 
                "Pegasus Ryūsei Ken - Meteoros de Pégaso", 
                "Pegasus Suisei Ken - Cometa de Pégaso", 
                "Pegasus Rolling Crush - Turbilhão de Pégaso"
            ],
            deus : "Athena"
        }
    
        let campoObrigatorio = ['nome', 'patente', 'descricao', 'ataques', 'deus'];
        let endpoint = 'test/responseErrors';
        let valorObrigatorio = validarErros.valorObrigatorio(cavaleiro,campoObrigatorio,endpoint);

        chai.expect(valorObrigatorio).to.containSubset(responseErrorsSchema);
    });
    

    it('arrayObrigatorio - O campo informado é um array',function(){
        let validarErros = new responseErrors;
        let cavaleiro = {    
            nome : "Seiya de Pégaso",
            patente : "bronze",
            descricao : "Seiya é o Cavaleiro de Bronze da constelação de Pegasus. Quando criança, Seiya foi separado de sua irmã, Seika, para fazer o treinamento para Cavaleiro no Santuário de Athena, na Grécia. Graças aos métodos rígidos e disciplinares de sua mentora Marin de Águia, Seiya foi capaz de cultivar um enorme potencial e conquistar a Armadura de Pégaso. Motivado pelo desejo de reencontrar sua irmã, Seiya depois descobre seu destino como um verdadeiro Cavaleiro de Athena, renascendo sempre que a deusa reencarna, para ajudá-la na batalha contra o mal que consome a Terra. Guerreiro de imenso poder, Seiya alcança a vitória em batalhas aparentemente impossíveis e consegue derrotar até mesmo os deuses Poseidon e Hades.",
            ataques : [ 
                "Pegasus Ryūsei Ken - Meteoros de Pégaso", 
                "Pegasus Suisei Ken - Cometa de Pégaso", 
                "Pegasus Rolling Crush - Turbilhão de Pégaso"
            ],
            deus : "Athena"
        }
    
        let campoObrigatorio = ['ataques'];
        let endpoint = 'test/responseErrors';
        let arrayObrigatorio = validarErros.arrayObrigatorio(cavaleiro,campoObrigatorio,endpoint);                        
        chai.expect(arrayObrigatorio).to.have.property('errors').with.empty
        chai.expect(arrayObrigatorio).to.have.property('title').with.null
        chai.expect(arrayObrigatorio).to.have.property('status').with.null
        chai.expect(arrayObrigatorio).to.have.property('detail').with.null
        chai.expect(arrayObrigatorio).to.have.property('instance').with.null
    })

    it('arrayObrigatorio - O campo informado NÃO é um array',function(){
        let validarErros = new responseErrors;
        let cavaleiro = {    
            nome : "Seiya de Pégaso",
            patente : "bronze",
            descricao : "Seiya é o Cavaleiro de Bronze da constelação de Pegasus. Quando criança, Seiya foi separado de sua irmã, Seika, para fazer o treinamento para Cavaleiro no Santuário de Athena, na Grécia. Graças aos métodos rígidos e disciplinares de sua mentora Marin de Águia, Seiya foi capaz de cultivar um enorme potencial e conquistar a Armadura de Pégaso. Motivado pelo desejo de reencontrar sua irmã, Seiya depois descobre seu destino como um verdadeiro Cavaleiro de Athena, renascendo sempre que a deusa reencarna, para ajudá-la na batalha contra o mal que consome a Terra. Guerreiro de imenso poder, Seiya alcança a vitória em batalhas aparentemente impossíveis e consegue derrotar até mesmo os deuses Poseidon e Hades.",
            ataques : "",
            deus : "Athena"
        }
    
        let campoObrigatorio = ['ataques'];
        let endpoint = 'test/responseErrors';
        let arrayObrigatorio = validarErros.arrayObrigatorio(cavaleiro,campoObrigatorio,endpoint);     
        
        chai.expect(arrayObrigatorio).to.containSubset(responseErrorsSchema);
    })

})