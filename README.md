

## 71 AOJ - ENG SW DEV - TRABALHO 2
Trabalho final da materia  **ENGINEERING SOFTWARE DEVELOPMENT**.  
Professor: **ANDRE PONTES SAMPAIO**  
Turma: **71AOJ**  

### API do Zodiaco
Esta API tem como obtetivo retornar informaçãos dos personagens da série "Saint Seyia - Os cavaleiros do zodiaco". Também é possivel cadastrar novas informações,editar informações e deletar informações.

Este trabalho contempla:
- Desenvolvimento em nodejs
- Utilização do Dockerfile e do docker-compose
- Banco de dados com mongodb
- Credenciais de acesso e JWT
- Teste unitário
- Teste integrado
- Integração com Swagger
- Front-end com funções crud

### Instruções

É possivel utilizar o [Plat With Docker](https://labs.play-with-docker.com/) ou executa-lo localmente

#### Com Play With Docker
**ATENÇÃO:** Por alguma razão o play-whit-docker não consegue se conecatar a base de dados (Recomendo fazer o procedimento localmente)

Acessar https://labs.play-with-docker.com/ e logar com sua conta do dockerhub. logar e adicionar uma nova instancia:

1. Clonar o repositório:  
```bash
git clone https://gitlab.com/iacornedi/api-fiap
```  
2. Acessar diretório:
```bash
cd api-fiap
```
3. Inciar aplicação:
```bash
docker-compose up -d
chmod +x restore.sh 
./restore.sh
```

Endereço de acesso: 
- [http://localhost/front/index.html](http://localhost/front/index.html)
- [http://localhost/api-docs/](http://localhost/api-docs/)