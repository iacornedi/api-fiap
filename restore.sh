#!/bin/bash
docker cp database/cavaleiros.json mongo:/
docker cp database/usuarios.json mongo:/
docker exec -d mongo mongoimport --db apidozodiaco --collection cavaleiros --file cavaleiros.json
docker exec -d mongo mongoimport --db apidozodiaco --collection usuarios --file usuarios.json