const server = require('./server');
const swaggerJSDoc = require('swagger-jsdoc')

const swaggerDefinition = {
    info:{
        title: 'apidozodicao',
        version: '1.0.0',
        description: 'Esta API retorna as informações dos personagens da série "Saint Seiya - Os Cavaleiros do Zodiaco"'
    },
    host: 'localhost',
    basePath: '/'
}

let options = {
    swaggerDefinition: swaggerDefinition,
    apis: ['./server.js']
}

let swaggerSpec = swaggerJSDoc(options);

server.get('swagger.json',function(req,res,next){
    res.send(swaggerSpec);
})
  
